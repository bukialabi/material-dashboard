# Generic Material Dashboard
This project is exactly what the title suggests - a generic dashboard created using [google material design](https://material.io/guidelines/material-design/introduction.html#introduction-principles). It achieves the material look using [materialize css](http://materializecss.com/) classes.


## Getting Started
Follow the instructions on the [sass website](http://sass-lang.com) to install Sass.

Clone the project from its git repository:
```
git clone https://bukialabi@gitlab.com/bukialabi/material-dashboard.git
```

Once cloned, install all the development dependencies on your computer by typing the following in your terminal:
```
npm install
```

See deployment notes below on how to deploy the project on a live system.

### Prerequisites

This project uses npm as a build tool, so you will need to have nodejs installed in order to take advantage of the various scripts built into the package. Learn more by visiting the [nodejs website](https://nodejs.org/en/).

For information on how to install node and update npm, visit the [npmjs website](https://docs.npmjs.com/getting-started/installing-node).


### Developing & Customizing the Dashboard
Run the following commands in your terminal to get started customizing the dashboard:

```
npm start
```
This will:
* Open the dashboard in your default browser
* Start a development server that watches the folder for changes and automatically reloads the browser as you code
* Watch all sass files created in the sass folder and auto-generate their css equivalents in the css folder

Happy coding!


```
npm run clean
```
This will:
* Remove extra files and folders auto-generated during development.


## Running the tests

(Nothing yet)

### Break down into end to end tests

(nothing yet)

### And coding style tests

(nothing yet)

## Deployment
Every folder except the following should be placed on the http server of you choice:
* The Node Modules folder
* The Sass Folder
* The package.json file
* The readme file
* The license file


## Built With

* [Materialize CSS](http://materializecss.com/) - The UI framework used
* [Sass](http://sass-lang.com) - Framework for extra styling

## Contributing

(Nothing Yet)


## Versioning

I use the most recent update date for versioning (format - YYYY.MM.DD).

## Authors

* **Adebanke Buki Alabi** - *Initial work* - [Generic Material Dashboard](gitlab.com/bukialabi/material-dashboard)

See also the list of [contributors]( ) who participated in this project.

## License

This project is licensed under the ISC License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

(Nothing Yet)
